Commande à exécuter depuis la machine vagrant une fois lancée:


```
#!shell

sudo apt-get dist-upgrade -y
```


créer une clef ssh sur le serveur pour que Jenkins puisse accéder au repo git
Il faut d’abord changer d’utilisateur, (se connecter en tant qu'utilisateur jenkins)

sudo su -s /bin/bash jenkins


Puis effectuer les étapes suivante permettant de créer une clé ssh

ssh -V 
ls -a ~/.ssh 
ssh-keygen 
ls -a ~/.ssh
ps -e | grep [s]sh-agent 
ssh-agent /bin/bash
ssh-add ~/.ssh/id_rsa 
ssh-add -l 
cat ~/.ssh/id_rsa.pub

Récupérer le résultat de la commande cat et le copier dans setting>key shh> du repo

et lancer

git ls-remote -h git@bitbucket.org:Ondariel/build.git HEAD
ou
ssh -T git@bitbucket.org

Finalement se connecter a Jenkins
puis dans Jenkins créer un Credentials entrer un nom d’utilisateur, sélectionner "From a file on Jenkins master" and entrer 
 /var/lib/jenkins/.ssh/id_rsa